/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtGui/QColorDialog>
#include <QtGui/QTextBlock>
#include <QtGui/QTextTable>
#include <QtGui/QTextList>
#include "dlgaddtask.h"

DlgAddTask::DlgAddTask(QWidget *parent, Qt::WindowFlags f): QDialog(parent, f), ui(new Ui::DlgAddTask), colorDialog(0)
{
	ui->setupUi(this);
	
	pageSetUp = false;
	ui->stackedWidget->setCurrentIndex(0);
	node = 0;
	edit = false;
	ui->rbtnClose->setChecked(true);
	
}

DlgAddTask::~DlgAddTask()
{
	delete ui;
}

void DlgAddTask::on_rbtnClose_clicked()
{
	node->setType(TYPE_NODE_CLOSE);
}

void DlgAddTask::on_rbtnOpen_clicked()
{
	node->setType(TYPE_NODE_OPEN);
}

void DlgAddTask::on_rbtnConform_clicked()
{
	node->setType(TYPE_NODE_CONFORMITY);
}

void DlgAddTask::on_rbtnReg_clicked()
{
	node->setType(TYPE_NODE_REGULATING);
}

void DlgAddTask::on_btnNext_clicked()
{
	QTextCursor cursor = ui->teRegAnswer->textCursor();
    if(pageSetUp)
	{
		node->clear();
		switch(node->getType())
		{
			case TYPE_NODE_CLOSE:
				setCloseNode();
				break;
			case TYPE_NODE_OPEN:
				setOpenNode();
				break;
			case TYPE_NODE_CONFORMITY:
				setConformNode();
				break;
			case TYPE_NODE_REGULATING:
				setRegNode();
				break;
			default:
				break;
		}
		
		ui->btnNext->setText(trUtf8("Далее >"));
		pageSetUp = false;
		clear();
		ui->stackedWidget->setCurrentIndex(0);
		ui->rbtnClose->setChecked(true);
		accept();
	}
	else
	{
		switch(node->getType())
		{
			case TYPE_NODE_CLOSE:
				ui->stackedWidget->setCurrentIndex(1);
				break;
			case TYPE_NODE_OPEN:
				ui->stackedWidget->setCurrentIndex(2);
				break;
			case TYPE_NODE_CONFORMITY:
				ui->stackedWidget->setCurrentIndex(3);
				break;
			case TYPE_NODE_REGULATING:
				cursor.createList(QTextListFormat::ListDecimal);
				ui->teRegAnswer->setTextCursor(cursor);
				ui->stackedWidget->setCurrentIndex(4);
				break;
			default:
				node->setType(TYPE_NODE_CLOSE);
				ui->stackedWidget->setCurrentIndex(1);
				break;
		}
		ui->btnNext->setText(trUtf8("Добавить"));
		pageSetUp = true;
	}
}

void DlgAddTask::setCloseNode()
{
	node->clear();
	node->setTaskTest(ui->leCloseTask->text());
	node->setQuestion(toHTML(ui->teCloseQuest));
	
	QStringList answList = toHTMLList(ui->lwCloseAnswers);
	for(int i = 0; i < answList.count(); i++)
	{
		QString tmp = answList.at(i);
		if(tmp.at(0) == '+') // ☑
		{
			node->addAnswer(tmp.remove(0, 2), 1);
		}
		else
		{
			node->addAnswer(tmp, 0);
		}
	}
}

void DlgAddTask::setOpenNode()
{
	node->clear();
	node->setTaskTest(ui->leOpenTask->text());
	node->setQuestion(toHTML(ui->teOpenQuest));
	
	for(int i = 0; i < ui->lwOpenAnswers->count(); i++)
	{
		QString tmp = ui->lwOpenAnswers->item(i)->text();
		node->addAnswer(tmp);
	}
}

void DlgAddTask::setConformNode()
{
	QTextCursor cursor = ui->twConformQuestAnsw->textCursor();
	QTextTable *table = cursor.currentTable();
    QString sreal = "";
	node->clear();
	node->setTaskTest(ui->leConformTask->text());
	int n = table->rows();
	for(int i = 0; i < n; i++)
	{
		QString text = toHTML(table->cellAt(i, 0).firstCursorPosition().block());
	    	if(text.isEmpty())
		{
			sreal += "0";
		}
		else
		{
			node->addQuestion(text);
			sreal += QString().setNum(i + 1);
		}
		
		text = toHTML(table->cellAt(i, 1).firstCursorPosition().block());
		if(!text.isEmpty())
			node->addAnswer(text);
	}
	if(sreal.length() < n)
		for(int i = 0; i < (n - sreal.length()); i++)
			sreal += QString().setNum(0);
	
		node->setReals(sreal);
}

void DlgAddTask::setRegNode()
{
	QString sreal = "";
	node->clear();
	node->setTaskTest(ui->leRegTask->text());
	
	QTextCursor cursor = ui->teRegAnswer->textCursor();
	QTextList *textList = cursor.currentList();
	
	for(int i = 0; i < textList->count(); i++)
	{
		node->addAnswer(toHTML(textList->item(i)));
		sreal += QString().setNum(i + 1);
	}
	node->setReals(sreal);
}

void DlgAddTask::on_btnOpenAddAnsw_clicked()
{
	ui->lwOpenAnswers->addItem(ui->leOpenAnswer->text());
	ui->leOpenAnswer->clear();
}

void DlgAddTask::on_btnConformAdd_clicked()
{
	QTextCursor cursor = ui->twConformQuestAnsw->textCursor();
	QTextTable *table;
	
	if(cursor.currentTable() == 0)
	{
	    table = cursor.insertTable(1, 2);
	}
	else
	{
	    table = cursor.currentTable();
	    table->appendRows(1);
	    cursor.movePosition(QTextCursor::NextRow);
	}
	
	cursor.insertHtml(toHTML(ui->teConformQuest));
	cursor.movePosition(QTextCursor::NextCell);
	cursor.insertHtml(toHTML(ui->teConformAnswer));
	cursor.movePosition(QTextCursor::PreviousCell);
	
	ui->twConformQuestAnsw->setTextCursor(cursor);
	
	ui->teConformQuest->clear();
	ui->teConformAnswer->clear();
}

TestNode * DlgAddTask::getTestNode() const
{
	return node;
}

void DlgAddTask::on_btnCancel_clicked()
{
	clear();
	pageSetUp = false;
	edit = false;
	ui->stackedWidget->setCurrentIndex(0);
	ui->btnNext->setText(trUtf8("Далее >"));
	ui->rbtnClose->setChecked(true);
	reject();
}

void DlgAddTask::setNode(TestNode *node)
{
	this->node = node;
}

void DlgAddTask::clear()
{
	ui->leCloseTask->clear();
	ui->leConformTask->clear();
	ui->leOpenTask->clear();
	ui->leRegTask->clear();
	
	ui->teCloseQuest->clear();
	ui->teOpenQuest->clear();
	ui->teConformQuest->clear();
	
	ui->leOpenAnswer->clear();
	ui->teConformAnswer->clear();
	ui->teRegAnswer->clear();
	
	ui->lwCloseAnswers->clear();
	ui->lwOpenAnswers->clear();
	ui->twConformQuestAnsw->clear();
}

void DlgAddTask::on_btnCloseEdit_clicked()
{
    ui->lwCloseAnswers->moveCursor(QTextCursor::StartOfBlock);
	ui->lwCloseAnswers->setCurrentCharFormat(QTextCharFormat());
    QTextCursor cursor = ui->lwCloseAnswers->textCursor();
    QString text = cursor.block().text();
    if(text.at(0) == '+')
    {
	cursor.deleteChar();
	cursor.deleteChar();
    }
    else
    {
	cursor.insertText("+ ");
    }
    ui->lwCloseAnswers->setTextCursor(cursor);
}

void DlgAddTask::on_lwOpenAnswers_clicked()
{
	ui->leOpenAnswer->setText(ui->lwOpenAnswers->currentItem()->text());
}

void DlgAddTask::on_btnOpenEdit_clicked()
{
	ui->lwOpenAnswers->currentItem()->setText(ui->leOpenAnswer->text());
}

void DlgAddTask::on_btnOpenDel_clicked()
{
	delete ui->lwOpenAnswers->currentItem();
	ui->leOpenAnswer->clear();
}

void DlgAddTask::on_btnConformDel_clicked()
{
	QTextCursor cursor = ui->twConformQuestAnsw->textCursor();
	QTextTable *table = cursor.currentTable();
	int index = table->cellAt(cursor).row();
	table->removeRows(index, 1);
}

void DlgAddTask::on_btnRegDel_clicked()
{
	QTextCursor cursor = ui->teRegAnswer->textCursor();
	QTextList *textList = cursor.currentList();
	textList->removeItem(textList->itemNumber(cursor.block()));
	cursor.select(QTextCursor::BlockUnderCursor);
	cursor.removeSelectedText();
	ui->teRegAnswer->setTextCursor(cursor);
}

int DlgAddTask::exec()
{
	if(edit)
	{
		clear();
		int nq, na, p;
		QString text;
		QTextCursor cursor;
		QTextTable *table;
		QStringList list;
		QString html;
		switch(node->getType())
		{
			case TYPE_NODE_CLOSE:
				ui->stackedWidget->setCurrentIndex(1);
				ui->leCloseTask->setText(node->getTask());
				ui->teCloseQuest->setText(node->getQuestion());
				text = QString();
				for(int i = 0; i < node->getAnswers().count(); i++)
				{
					text += "<p>";
					int n = qPow(2, i);
					if((n & node->getRealsInt()) == 0)
						text += node->getAnswers().at(i);
					else
						text += "+ " + node->getAnswers().at(i);
					text += "</p>";
				}
				ui->lwCloseAnswers->setHtml(text);
				break;
			case TYPE_NODE_OPEN:
				ui->stackedWidget->setCurrentIndex(2);
				ui->leOpenTask->setText(node->getTask());
				ui->teOpenQuest->setText(node->getQuestion());
				ui->lwOpenAnswers->addItems(node->getAnswers());
				break;
			case TYPE_NODE_CONFORMITY:
				ui->stackedWidget->setCurrentIndex(3);
				ui->leConformTask->setText(node->getTask());
				nq = node->getQuestions().count();
				na = node->getAnswers().count();
				p = 0;
				cursor = ui->twConformQuestAnsw->textCursor();
				table = cursor.insertTable(na, 2);
				for(int i = 0; i < nq; i++)
				{
					cursor.insertHtml(node->getQuestions().at(i));
					cursor.movePosition(QTextCursor::NextCell, QTextCursor::MoveAnchor, 2);
					p += 2;
				}
				if(nq == na)
				    cursor.movePosition(QTextCursor::PreviousCell, QTextCursor::MoveAnchor, p - 2);
				else
				    cursor.movePosition(QTextCursor::PreviousCell, QTextCursor::MoveAnchor, p - 1);
				for(int i = 0; i < na; i++)
				{
					cursor.insertHtml(node->getAnswers().at(i));
					cursor.movePosition(QTextCursor::NextCell, QTextCursor::MoveAnchor, 2);
				}
				cursor.movePosition(QTextCursor::PreviousCell);
				ui->twConformQuestAnsw->setTextCursor(cursor); 
				break;
			case TYPE_NODE_REGULATING:
				ui->stackedWidget->setCurrentIndex(4);
				ui->leRegTask->setText(node->getTask());
				list = node->getAnswers();
				html = "<ol>";
				for(int i = 0; i < list.count(); i++)
				{
				    html += QString("<li>%1</li>").arg(list.at(i));
				}
				html += "</ol>";
				ui->teRegAnswer->setHtml(html);
				break;
			default:
				node->setType(TYPE_NODE_CLOSE);
				ui->stackedWidget->setCurrentIndex(1);
				break; 
		}
		
		ui->btnNext->setText(trUtf8("Изменить"));
		pageSetUp = true;
		edit = false;
	}
	return QDialog::exec();
}

void DlgAddTask::setEdit(bool edit)
{
	this->edit = edit;
}

void DlgAddTask::on_actionBold_toggled(bool on)
{
    
	QTextEdit *curEdit = qobject_cast< QTextEdit* >(focusWidget());
	if(curEdit)
	    curEdit->setFontWeight(on ? QFont::Bold : QFont::Normal);
}

void DlgAddTask::on_actionColor_triggered()
{
    QTextEdit *curEdit = qobject_cast< QTextEdit* >(focusWidget());
    if(curEdit)
    {
	if(!colorDialog)
	{
	    colorDialog = new QColorDialog(this);
	    connect(colorDialog, SIGNAL(colorSelected(const QColor&)), this, SLOT(updateColor(const QColor&)));
	}
  
	colorDialog->setCurrentColor(curEdit->textColor());
	colorDialog->open();
    }
}

void DlgAddTask::on_actionItalic_toggled(bool on)
{
    QTextEdit *curEdit = qobject_cast< QTextEdit* >(focusWidget());
    if(curEdit)
	curEdit->setFontItalic(on);
}

void DlgAddTask::on_actionSubscript_toggled(bool on)
{
    QTextEdit *curEdit = qobject_cast< QTextEdit* >(focusWidget());
    if(curEdit)
    {
	QTextCharFormat format;
	format.setVerticalAlignment(on ? QTextCharFormat::AlignSubScript : QTextCharFormat::AlignNormal);
	curEdit->mergeCurrentCharFormat(format);
    }
}

void DlgAddTask::on_actionSuperscript_toggled(bool on)
{
    QTextEdit *curEdit = qobject_cast< QTextEdit* >(focusWidget());
    if(curEdit)
    {
	QTextCharFormat format;
	format.setVerticalAlignment(on ? QTextCharFormat::AlignSuperScript : QTextCharFormat::AlignNormal);
	curEdit->mergeCurrentCharFormat(format);
    }
}

void DlgAddTask::on_actionUnderline_toggled(bool on)
{
    QTextEdit *curEdit = qobject_cast< QTextEdit* >(focusWidget());
    if(curEdit)
	curEdit->setFontUnderline(on);
}

void DlgAddTask::updateColor(const QColor &color)
{
    QTextEdit *curEdit = qobject_cast< QTextEdit* >(focusWidget());
    if(curEdit)
	curEdit->setTextColor(color);
}

QString DlgAddTask::toHTML(QTextEdit* textEdit) const
{
    QString html;
    for(QTextBlock block = textEdit->document()->begin(); block.isValid(); block = block.next())
    {
	for(QTextBlock::iterator i = block.begin(); !i.atEnd(); ++i)
	{
	    QTextFragment fragment = i.fragment();
	    if(fragment.isValid())
	    {
		QTextCharFormat format = fragment.charFormat();
		QColor color = format.foreground().color();
		QString text = Qt::escape(fragment.text());
		QStringList tags;
		if(format.verticalAlignment() == QTextCharFormat::AlignSubScript)
		    tags << "sub";
		else if(format.verticalAlignment() == QTextCharFormat::AlignSuperScript)
		    tags << "sup";
		if(format.fontItalic())
		    tags << "i";
		if(format.fontWeight() > QFont::Normal)
		    tags << "b";
		if(format.fontUnderline())
		    tags << "ins";
		while(!tags.isEmpty())
		    text = QString("<%1>%2</%1>").arg(tags.takeFirst()).arg(text);
		if(color != Qt::black)
		    text = QString("<font color=\"%1\">%2</font>").arg(color.name()).arg(text);
		html += text;
	    }
	}
    }
    return html;
}

QString DlgAddTask::toHTML(QTextBlock block) const
{
    QString html;
    if(block.text().isEmpty())
	return QString();
    
    for(QTextBlock::iterator i = block.begin(); !i.atEnd(); ++i)
    {
	QTextFragment fragment = i.fragment();
	if(fragment.isValid())
	{
	    QTextCharFormat format = fragment.charFormat();
	    QColor color = format.foreground().color();
	    QString text = Qt::escape(fragment.text());
	    QStringList tags;
	    if(format.verticalAlignment() == QTextCharFormat::AlignSubScript)
		tags << "sub";
	    else if(format.verticalAlignment() == QTextCharFormat::AlignSuperScript)
		tags << "sup";
	    if(format.fontItalic())
		tags << "i";
	    if(format.fontWeight() > QFont::Normal)
		tags << "b";
	    if(format.fontUnderline())
		tags << "ins";
	    while(!tags.isEmpty())
		text = QString("<%1>%2</%1>").arg(tags.takeFirst()).arg(text);
	    if(color != Qt::black)
		text = QString("<font color=\"%1\">%2</font>").arg(color.name()).arg(text);
	    html += text;
	}
    }
    return html;
}

void DlgAddTask::changedFormat(const QTextCharFormat& format)
{
    ui->actionBold->setChecked(format.fontWeight() == QFont::Bold);
    ui->actionItalic->setChecked(format.fontItalic());
    ui->actionUnderline->setChecked(format.fontUnderline());
    ui->actionSubscript->setChecked(format.verticalAlignment() == QTextCharFormat::AlignSubScript);
    ui->actionSuperscript->setChecked(format.verticalAlignment() == QTextCharFormat::AlignSuperScript);
}

void DlgAddTask::on_lwCloseAnswers_currentCharFormatChanged(const QTextCharFormat& format)
{
	changedFormat(format);
}

void DlgAddTask::on_teCloseQuest_currentCharFormatChanged(const QTextCharFormat& format)
{
    changedFormat(format);
}

void DlgAddTask::on_teConformAnswer_currentCharFormatChanged(const QTextCharFormat& format)
{
    changedFormat(format);
}

void DlgAddTask::on_teConformQuest_currentCharFormatChanged(const QTextCharFormat& format)
{
    changedFormat(format);
}

void DlgAddTask::on_teOpenQuest_currentCharFormatChanged(const QTextCharFormat& format)
{
    changedFormat(format);
}

void DlgAddTask::on_teRegAnswer_currentCharFormatChanged(const QTextCharFormat& format)
{
    changedFormat(format);
}

void DlgAddTask::on_twConformQuestAnsw_currentCharFormatChanged(const QTextCharFormat& format)
{
    changedFormat(format);
}

QStringList DlgAddTask::toHTMLList(QTextEdit* textEdit) const
{
    QStringList html;
    for(QTextBlock block = textEdit->document()->begin(); block.isValid(); block = block.next())
    {
	QString textFragment;
	for(QTextBlock::iterator i = block.begin(); !i.atEnd(); ++i)
	{
	    QTextFragment fragment = i.fragment();
	    if(fragment.isValid())
	    {
		QTextCharFormat format = fragment.charFormat();
		QColor color = format.foreground().color();
		QString text = Qt::escape(fragment.text());
		QStringList tags;
		if(format.verticalAlignment() == QTextCharFormat::AlignSubScript)
		    tags << "sub";
		else if(format.verticalAlignment() == QTextCharFormat::AlignSuperScript)
		    tags << "sup";
		if(format.fontItalic())
		    tags << "i";
		if(format.fontWeight() > QFont::Normal)
		    tags << "b";
		if(format.fontUnderline())
		    tags << "ins";
		while(!tags.isEmpty())
		    text = QString("<%1>%2</%1>").arg(tags.takeFirst()).arg(text);
		if(color != Qt::black)
		    text = QString("<font color=\"%1\">%2</font>").arg(color.name()).arg(text);
		textFragment += text;
	    }
	}
	html.append(textFragment);
    }
    return html;
}

#include "dlgaddtask.moc"